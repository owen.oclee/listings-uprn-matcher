from typing import Dict, List
from matching import MatchCandidate, MatchOutcome
from util import format_address_parts


def assert_matches_equal(
    expected_matches: List[MatchOutcome], actual_matches: List[MatchOutcome]
) -> None:
    expected_listing_str_to_candidate: Dict[str, MatchCandidate] = {}
    for match_outcome in expected_matches:
        expected_listing_str_to_candidate[
            match_outcome.listing.string()
        ] = match_outcome.match_candidate

    passes = 0
    for actual_match in actual_matches:
        expected_candidate = expected_listing_str_to_candidate[
            actual_match.listing.string()
        ]
        listing_address = format_address_parts(
            actual_match.listing.json(include_coordinates=False), lowercase=False
        )
        expected_address = format_address_parts(
            expected_candidate.multi_line_address, lowercase=False
        )
        actual_address = format_address_parts(
            actual_match.match_candidate.multi_line_address, lowercase=False
        )

        if expected_candidate.uprn == actual_match.match_candidate.uprn:
            passes += 1
        else:
            print()
            print("##### Fail #####")
            print(listing_address)
            print(f"Expected: {expected_address} ({expected_candidate.uprn})")
            print(f"Actual: {actual_address} ({actual_match.match_candidate.uprn})")
            if actual_match.match_candidate.match_reason:
                print(
                    f"Matched using: {actual_match.match_candidate.match_reason.name}"
                )
            print()

    percentage = (passes / len(expected_matches)) * 100
    print(
        f"{passes}/{len(expected_matches)} ({percentage:.2f}%) matches against ground truth"
    )
