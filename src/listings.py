from __future__ import annotations
import re
import csv
from typing import List

SAFE_DELIM = "Ψ"
POSTCODE_REGEX = re.compile("^[A-Z]{1,2}\d[A-Z\d]? ?\d[A-Z]{2}$")


class Listing:
    def __init__(
        self,
        property_number: str,
        street_address: str,
        postcode: str,
        latitude: str,
        longitude: str,
    ):
        if not POSTCODE_REGEX.match(postcode):
            raise ValueError("Invalid postcode format")
        self.property_number = property_number
        self.street_address = street_address
        self.postcode = postcode
        self.latitude = latitude
        self.longitude = longitude

    @classmethod
    def from_json(cls, row: List[str]) -> Listing:
        return Listing(row[0], row[1], row[2], row[3], row[4])

    @classmethod
    def from_string(cls, s: str) -> Listing:
        return Listing.from_json(s.split(SAFE_DELIM))

    def json(self, include_coordinates: bool = True) -> List[str]:
        return [
            self.property_number,
            self.street_address,
            self.postcode,
        ] + ([self.latitude, self.longitude] if include_coordinates else [])

    def string(self) -> str:
        return self.__str__()

    def __str__(self) -> str:
        return SAFE_DELIM.join(self.json())


def load_listings(data_path: str) -> List[Listing]:
    listings: List[Listing] = []
    with open(data_path) as file:
        reader = csv.reader(file)
        next(reader)  # skip header
        for row in reader:
            listings.append(Listing.from_json(row))
    return listings
