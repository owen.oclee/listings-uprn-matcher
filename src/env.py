import os
from dotenv import load_dotenv


class Env:
    def __init__(self) -> None:
        load_dotenv()
        self.api_address_data_url = os.getenv("API_ADDRESS_DATA_URL", "")
        self.data_path = os.getenv("DATA_PATH", "")
        self.ground_truth_path = os.getenv("GROUND_TRUTH_PATH", "")

        if not self.api_address_data_url or not self.api_address_data_url.endswith("/"):
            raise ValueError(
                "API_ADDRESS_DATA_URL must be supplied and end with a forward slash"
            )
        if not self.data_path:
            raise ValueError("DATA_PATH must be supplied")
        if not self.ground_truth_path:
            raise ValueError("GROUND_TRUTH_PATH must be supplied")
