import json
from typing import List
from matching import MatchOutcome


def load_ground_truth_json(ground_truth_path: str) -> List[MatchOutcome]:
    matches: List[MatchOutcome] = []
    with open(ground_truth_path) as file:
        ground_truth_json = json.load(file)
        for match_json in ground_truth_json:
            match = MatchOutcome.from_json(match_json)
            matches.append(match)
    return matches


def write_ground_truth_json(
    ground_truth_path: str, matches: List[MatchOutcome]
) -> None:
    with open(ground_truth_path, "w") as file:
        json_list = [match.json() for match in matches]
        json.dump(json_list, file, indent=4)
        file.write("\n")
