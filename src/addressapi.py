from __future__ import annotations
import requests
from typing import Any, Dict, List, Union


class BadAPIResponse(Exception):
    pass


class Address:
    def __init__(
        self,
        building_name: Union[str, None],
        latitude: float,
        longitude: float,
        multi_line_address: List[str],
        number: Union[int, None],
        street_name: str,
        sub_building_name: Union[str, None],
        uprn: str,
    ):
        self.building_name = building_name
        self.latitude = latitude
        self.longitude = longitude
        self.multi_line_address = multi_line_address
        self.number = number
        self.street_name = street_name
        self.sub_building_name = sub_building_name
        self.uprn = uprn

    @classmethod
    def from_json(cls, json: Dict[str, Any]) -> Address:
        return cls(
            json["building_name"],
            json["latitude"],
            json["longitude"],
            json["multi_line_address"],
            json["number"],
            json["street_name"],
            json["sub_building_name"],
            json["uprn"],
        )

    def json(self) -> Dict[str, Any]:
        return {
            "building_name": self.building_name,
            "latitude": self.latitude,
            "longitude": self.longitude,
            "multi_line_address": self.multi_line_address,
            "number": self.number,
            "street_name": self.street_name,
            "sub_building_name": self.sub_building_name,
            "uprn": self.uprn,
        }


class AddressService:
    def __init__(self, api_address_data_url: str):
        self.api_address_data_url = api_address_data_url
        if not api_address_data_url.endswith("/"):
            raise ValueError("api_address_data_url must end with a forward slash")

    def addresses_for_postcode(self, postcode: str) -> List[Address]:
        clean_postcode = postcode.upper().replace(" ", "%20")
        res = requests.get(f"{self.api_address_data_url}postcodes/{clean_postcode}")
        if res.status_code == 404:
            return []
        if res.status_code != 200:
            raise BadAPIResponse(f"API responded with {res.status_code}")
        return [Address.from_json(address) for address in res.json()["addresses"]]

    def address_for_uprn(self, uprn: str) -> Union[Address, None]:
        print(f"{self.api_address_data_url}uprn/{uprn}")
        res = requests.get(f"{self.api_address_data_url}uprn/{uprn}")
        if res.status_code == 404:
            return None
        if res.status_code != 200:
            raise BadAPIResponse(f"API responded with {res.status_code}")
        return Address.from_json(res.json()["address"])
