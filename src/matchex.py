from __future__ import annotations

from addressapi import AddressService
from env import Env
from groundtruth import load_ground_truth_json
from matchers import find_match
from testing import assert_matches_equal


if __name__ == "__main__":
    env = Env()
    address_service = AddressService(env.api_address_data_url)

    expected_matches = load_ground_truth_json(env.ground_truth_path)
    actual_matches = []

    for expected_match in expected_matches:
        match = find_match(address_service, expected_match.listing)
        if match.match_candidate.match_reason:
            print(
                f"{match.match_candidate.match_reason.name}: {match.match_candidate.uprn}"
            )
        actual_matches.append(match)

    assert_matches_equal(expected_matches, actual_matches)
