from __future__ import annotations
from enum import Enum, auto
from typing import Any, Dict, List, Union

from listings import Listing


class MatchReason(Enum):
    NO_ADDRESSES_IN_POSTCODE = auto()
    FIRST_LINE_ADDR_EXCLUSIVELY_MATCH = auto()
    NUMBER_AND_STREET_EXACTLY_AND_EXCLUSIVELY_MATCH = auto()
    NUMBERS_EXCLUSIVELY_MATCH = auto()
    NONSENSE_INPUT = auto()
    NO_UPRN_FOR_ADDRESS = auto()

    @staticmethod
    def from_json(json: str) -> MatchReason:
        return MatchReason[json]

    def json(self) -> str:
        return self.name


class MatchCandidate:
    def __init__(
        self,
        uprn: Union[str, None],
        multi_line_address: Union[List[str], None],
        match_reason: Union[MatchReason, None],
    ):
        self.uprn = uprn
        self.multi_line_address = multi_line_address
        self.match_reason = match_reason

    @classmethod
    def from_json(cls, json: Dict[str, Any]) -> MatchCandidate:
        return MatchCandidate(
            json["uprn"] if "uprn" in json else None,
            json["multi_line_address"] if "multi_line_address" in json else None,
            MatchReason.from_json(json["match_reason"])
            if "match_reason" in json
            else None,
        )

    def json(self) -> Dict[str, Any]:
        json_obj: Dict[str, Any] = {
            "uprn": self.uprn,
        }
        if self.match_reason:
            json_obj["match_reason"] = self.match_reason.json()
        if self.multi_line_address:
            json_obj["multi_line_address"] = self.multi_line_address
        return json_obj


class MatchOutcome:
    def __init__(self, listing: Listing, match_candidate: MatchCandidate):
        self.listing = listing
        self.match_candidate = match_candidate

    @classmethod
    def from_json(cls, json: Dict[str, Any]) -> MatchOutcome:
        return MatchOutcome(
            Listing.from_json(json["listing"]),
            MatchCandidate.from_json(json["match_candidate"]),
        )

    def json(self) -> Dict[str, Any]:
        return {
            "listing": self.listing.json(),
            "match_candidate": self.match_candidate.json(),
        }

    def is_match(self) -> bool:
        return self.match_candidate.uprn != None
