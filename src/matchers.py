from nis import match
import re
from typing import Callable, List, Union

from addressapi import Address, AddressService
from listings import Listing
from matching import MatchCandidate, MatchOutcome, MatchReason
from util import format_address_parts


Matcher = Callable[[Listing, Listing, List[Address]], Union[MatchOutcome, None]]

def first_line_addr_exclusively_match(
    preprocessed_listing: Listing, listing: Listing, candidates: List[Address]
):
    matching_candidates: List[Address] = []
    for candidate in candidates:
        listing_addr = " ".join([preprocessed_listing.property_number, preprocessed_listing.street_address]).replace(",", "")
        candidate_first_line_addr = candidate.multi_line_address[0].lower().strip().replace(",", "")
        candidate_second_line_addr = candidate.multi_line_address[1].lower().strip().replace(",", "")
        candidate_first_and_second_line_addr = " ".join([candidate_first_line_addr, candidate_second_line_addr])
        if listing_addr == candidate_first_line_addr or listing_addr in candidate_first_and_second_line_addr or candidate_first_and_second_line_addr in listing_addr:
            matching_candidates.append(candidate)
    
    if len(matching_candidates) == 1:
        return MatchOutcome(
            listing,
            MatchCandidate(
                matching_candidates[0].uprn,
                matching_candidates[0].multi_line_address,
                MatchReason.FIRST_LINE_ADDR_EXCLUSIVELY_MATCH,
            ),
        )

    return None


def number_and_street_exactly_and_exclusively_match(
    preprocessed_listing: Listing, listing: Listing, candidates: List[Address]
) -> Union[MatchOutcome, None]:
    number_and_street_matching_candidates: List[Address] = []
    for candidate in candidates:
        if (
            str(candidate.number) == preprocessed_listing.property_number
            and candidate.street_name.lower().strip()
            == preprocessed_listing.street_address
        ):
            number_and_street_matching_candidates.append(candidate)

    if len(number_and_street_matching_candidates) == 1:
        return MatchOutcome(
            listing,
            MatchCandidate(
                number_and_street_matching_candidates[0].uprn,
                number_and_street_matching_candidates[0].multi_line_address,
                MatchReason.NUMBER_AND_STREET_EXACTLY_AND_EXCLUSIVELY_MATCH,
            ),
        )

    return None


def numbers_exclusively_match(
    preprocessed_listing: Listing, listing: Listing, candidates: List[Address]
) -> Union[MatchOutcome, None]:
    listing_address = format_address_parts(
        [preprocessed_listing.property_number, preprocessed_listing.street_address]
    )
    numbers_in_listing = re.findall("\d+[a-z]?", listing_address.lower())

    final_candidates_by_matching_numbers = {}

    for candidate in candidates:
        candidate_address = format_address_parts(candidate.multi_line_address[:-1])
        numbers_in_candidate_address = re.findall("\d+", candidate_address.lower())

        if len(numbers_in_listing) > len(numbers_in_candidate_address):
            continue

        numbers_in_listing_copy = numbers_in_listing.copy()
        matching_numbers = 0
        for number_in_candidate in numbers_in_candidate_address:
            if number_in_candidate in numbers_in_listing_copy:
                numbers_in_listing_copy.remove(number_in_candidate)
                matching_numbers += 1

        if matching_numbers == 0:
            continue

        if matching_numbers not in final_candidates_by_matching_numbers:
            final_candidates_by_matching_numbers[matching_numbers] = []
        final_candidates_by_matching_numbers[matching_numbers].append(candidate)

    if len(final_candidates_by_matching_numbers.keys()) == 0:
        return None
    
    highest_matching_numbers = sorted(final_candidates_by_matching_numbers.keys())[-1]
    if len(final_candidates_by_matching_numbers[highest_matching_numbers]) == 1:
        return MatchOutcome(
            listing,
            MatchCandidate(
                final_candidates_by_matching_numbers[highest_matching_numbers][0].uprn,
                final_candidates_by_matching_numbers[highest_matching_numbers][0].multi_line_address,
                MatchReason.NUMBERS_EXCLUSIVELY_MATCH,
            ),
        )

    return None


MATCHERS: List[Matcher] = [
    first_line_addr_exclusively_match,
    number_and_street_exactly_and_exclusively_match,
    numbers_exclusively_match,
]


def find_match(
    address_service: AddressService,
    listing: Listing,
    matchers: List[Matcher] = MATCHERS,
) -> MatchOutcome:
    candidates = address_service.addresses_for_postcode(listing.postcode)
    print(f"fetched addresses for postcode: {listing.postcode}")
    if len(candidates) == 0:
        return MatchOutcome(
            listing, MatchCandidate(None, None, MatchReason.NO_ADDRESSES_IN_POSTCODE)
        )
    
    # pre-process listing
    preprocessed_listing = Listing.from_string(listing.string())
    preprocessed_listing.property_number = preprocessed_listing.property_number.strip().lower()
    preprocessed_listing.street_address = preprocessed_listing.street_address.strip().lower()
    if listing.property_number == listing.street_address:
        preprocessed_listing.property_number = ""
    
    # fail early if the listing is obviously rubbish
    addr = preprocessed_listing.property_number + preprocessed_listing.street_address
    if re.match("^[^a-z0-9]*$", addr):
        return MatchOutcome(
            listing, MatchCandidate(None, None, MatchReason.NONSENSE_INPUT)
        )


    match_outcome = None
    for matcher in matchers:
        match_outcome = matcher(preprocessed_listing, listing, candidates)
        if match_outcome != None:
            break

    return match_outcome or MatchOutcome(
        listing, MatchCandidate(None, None, None)
    )
