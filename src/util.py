from typing import List, Union


def format_address_parts(
    address_parts: Union[List[str], None], lowercase: bool = True
) -> str:
    if address_parts == None:
        return ""
    sanitised_parts = [
        (part.lower() if lowercase else part).strip()
        for part in address_parts or []
        if part.strip() != "" and part.strip() != "."
    ]
    return ", ".join(sanitised_parts)
