from enum import Enum, auto
import random
from typing import Dict, List, Tuple
from addressapi import AddressService
from env import Env
from groundtruth import load_ground_truth_json, write_ground_truth_json
from listings import load_listings
from matching import MatchCandidate, MatchOutcome, MatchReason
from thefuzz import fuzz, process

from util import format_address_parts


class Choice(Enum):
    NO_CHOICE = auto()
    QUIT = auto()
    MORE = auto()
    SKIP = auto()
    UNMATCHABLE = auto()
    MATCH = auto()
    NO_UPRN = auto()


def user_choice(allow_more: bool) -> Tuple[Choice, int]:
    more_text = " 'm' to show more." if allow_more else ""
    while True:
        user_input = input(
            f"pick a number to confirm a match. 'u' to mark as unmatchable. 'n' to mark as not found. {more_text} 's' to skip. 'q' to quit.\n"
        )
        if user_input == "q":
            return (Choice.QUIT, 0)
        if user_input == "n":
            return (Choice.NO_UPRN, 0)
        if allow_more and user_input == "m":
            return (Choice.MORE, 0)
        if user_input == "s":
            return (Choice.SKIP, 0)
        if user_input == "u":
            return (Choice.UNMATCHABLE, 0)
        if user_input.isdigit():
            return (Choice.MATCH, int(user_input))


if __name__ == "__main__":
    env = Env()
    address_service = AddressService(env.api_address_data_url)

    expected_matches = load_ground_truth_json(env.ground_truth_path)
    listings_strings_in_ground_truth: Dict[str, bool] = {}
    for match in expected_matches:
        listings_strings_in_ground_truth[match.listing.string()] = True

    listings = load_listings(env.data_path)

    seen_indexes: Dict[int, bool] = {}
    index = random.randint(0, len(listings) + 1)
    option = 0
    manual_matches: List[MatchOutcome] = []
    while len(seen_indexes) < len(listings):
        # re-roll until we find something we haven't looked at before
        while index in seen_indexes:
            index = random.randint(0, len(listings)-1)
        seen_indexes[index] = True

        listing = listings[index]

        # if this listing already exists in the ground truth, skip
        if listing.string() in listings_strings_in_ground_truth:
            continue

        candidates = address_service.addresses_for_postcode(listing.postcode)
        # no candidates, nothing to manually match on, skip
        if len(candidates) == 0:
            continue

        listing_address = format_address_parts(listing.json()[0:2])
        choices = [
            (candidate.multi_line_address, candidate.uprn) for candidate in candidates
        ]
        print()
        print(listing_address)
        best_matches = process.extractBests(
            (listing.json()[0:2], ""),
            choices,
            processor=lambda c: format_address_parts(c[0]),
            scorer=fuzz.token_set_ratio,
            limit=None,
        )

        option = 0
        choice = Choice.NO_CHOICE
        selected_option = 0
        for ([address, uprn], score) in best_matches:
            option += 1
            print(f"#{option}: {format_address_parts(address)}")
            if option >= 5 and option % 5 == 0:
                (choice, selected_option) = user_choice(True)
                if choice == Choice.MORE:
                    print()
                    print(listing_address)
                    continue
                else:
                    break

        if choice == Choice.NO_CHOICE or choice == Choice.MORE:
            (choice, selected_option) = user_choice(False)
        if choice == Choice.QUIT:
            break
        elif choice == Choice.SKIP:
            continue
        elif choice == Choice.NO_UPRN:
            manual_matches.append(
                MatchOutcome(
                    listing, MatchCandidate(None, None, MatchReason.NO_UPRN_FOR_ADDRESS)
                )
            )
        else:
            if choice == Choice.UNMATCHABLE:
                manual_matches.append(
                    MatchOutcome(listing, MatchCandidate(None, None, None))
                )
            elif choice == Choice.MATCH:
                ([candidate_address, candidate_uprn], score) = best_matches[
                    selected_option - 1
                ]
                manual_matches.append(
                    MatchOutcome(
                        listing, MatchCandidate(candidate_uprn, candidate_address, None)
                    )
                )
            else:
                # should never happen
                print("unknown selection, skipping...")
                continue

    while True and len(manual_matches) > 0:
        yn = (
            input(
                f"write {len(manual_matches)} matches to {env.ground_truth_path}? (Y/n)\n"
            )
            .lower()
            .strip()
        )
        if yn == "" or yn == "y":
            write_ground_truth_json(
                env.ground_truth_path, expected_matches + manual_matches
            )
            break
        elif yn == "n":
            break
