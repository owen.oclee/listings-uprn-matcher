# Listings UPRN Matcher

This project aims to match listings address data, such as:

```
Listing Property Number     Listing Street Name     Listing Postal Code
19                          Kennedy Grove           B30 2QL
```

to its (ideally accurate!) UPRN `100070421040`:

```json
{
  "address": {
    "administrative_area": "BIRMINGHAM",
    "building_name": null,
    "country": "England",
    "dd_locality_name": null,
    "dep_street_name": null,
    "dependent_locality": null,
    "exclude_from_search": false,
    "incode": "2QL",
    "latitude": 52.4265369,
    "locality_name": null,
    "longitude": -1.9151091,
    "multi_line_address": [
      "19 Kennedy Grove",
      "Birmingham",
      "B30 2QL"
    ],
    "number": 19,
    "outcode_comp": "B30",
    "outcode": "B30",
    "post_town_name": "BIRMINGHAM",
    "postal_area": "B",
    "postcode_sector": "B30 2",
    "postcode": "B30 2QL",
    "sector": "B30 2",
    "single_line_address": "19 Kennedy Grove, Birmingham, B30 2QL",
    "street_name": "KENNEDY GROVE",
    "sub_building_name": null,
    "town_name": "BIRMINGHAM",
    "uprn": "100070421040",
    "usrn": "2703350"
  }
}
```

Listings are submitted by external sources so the data can sometimes be of
very low quality. This project should account for this and attempt to match
addresses even when there is not much to go on, perhaps with a measure of
confidence returned to the consumer in situations where a match is found based
on poor input.

## Setup

Install dependencies via [poetry](https://python-poetry.org/):

```
poetry install
```

Bring your own data sample called `data.csv` at the root of the
project, with the following headers:

```
Listing Property Number,Listing Street Name,Listing Postal Code,Location Coordinates Latitude,Location Coordinates Longitude
```

Configure .env:

```
mv .env.example .env
vim .env
```

Run it:

```
poetry run src/manualmatch.py
poetry run src/matchex.py
```

## Development

Keep things tidy:

```
poetry run black src/
poetry run mypy --strict --warn-unreachable src/
```
